import React from 'react';
import logo from '../logo.svg';
import '../index.scss';
import ObjectsList from './ObjectsList';

const HomePage = () => (
    <div className="row">
        <ObjectsList/>
    </div>
);

export default HomePage;
