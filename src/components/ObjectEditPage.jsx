import React, {Component} from 'react';
import {connect} from 'react-redux';

class ObjectEditPage extends Component {
    constructor(props){
        if(!props.object){
            props.history.push('/');
        }

        super(props);
    }

    render = () => (
        <div>
            {this.props.object && this.props.object.id}
        </div>
    );
}

const mapStateToProps = (state, props) => {
  return {
      object: state.objects.find( object => {
          return props.match.params.id === object.id
      })
  }
};

export default connect(mapStateToProps)(ObjectEditPage);