import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import selectObjects from '../selectors/objects'

const ObjectsList = (props) => (
    <ul>
        {props.objects.map((object, index) => (
            <li key={index}>
                <Link to={`/edit/${object.id}`}>
                    {object.description}
                </Link>
            </li>
        ))}
    </ul>
);

const mapStateToProps = (state) => ({objects: selectObjects(state.objects, state.filters)});

export default connect(mapStateToProps)(ObjectsList);