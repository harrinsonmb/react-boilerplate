import React from 'react';

const NotFoundPage = () => {
    return (
        <div>
            Nothing found here
        </div>
    );
};

export default NotFoundPage;