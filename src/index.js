import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import * as serviceWorker from './serviceWorker';
import AppRouter from "./routers/AppRouter";
import configureStore from "./store/configureStore";
import {addObject} from "./actions/object";
import getVisibleObjects from "./selectors/objects";

const store = configureStore();

store.subscribe(()=>{
    const state = store.getState();
    const visibleObjects = getVisibleObjects(state.objects, state.filters);
    console.log(visibleObjects);
});

store.dispatch(addObject({
    description: 'Primer objeto',
    is_public: true,
    weight: 20
}));

store.dispatch(addObject({
    description: 'Segundo objeto',
    is_public: false,
    weight: 10
}));

const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

ReactDOM.render(jsx, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
