import uuid from 'uuid';

const addObject = ({
        description = '',
        is_public = true,
        weight = 0,
    }) => ({
        type: 'ADD_OBJECT',
        object: {
            id: uuid(),
            createdAt: new Date(),
            description,
            is_public,
            weight
}});

const removeObject = ( {id}) => ({
    type: 'REMOVE_OBJECT',
    id
});

const updateObject = ({id, updates}) => ({
    type: 'UPDATE_OBJECT',
    id,
    updates
});

export {addObject, removeObject, updateObject}