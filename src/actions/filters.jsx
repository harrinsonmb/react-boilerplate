
const setTextFilter = (text= '') => ({
    type: 'SET_TEXT_FILTER',
    text
});

const resetAllFilters = () => ({
   type: 'RESET_FILTERS'
});

const setVisibilityFilter = (privateObjectsHidden = false) => ({
    type: 'SET_VISIBILITY',
    privateObjectsHidden
});

const sortByWeight = () => ({
    type: 'SORT_BY_WEIGHT'
});

const sortByDate = () => ({
    type: 'SORT_BY_DATE'
});

const setStartDate = (startDate) => ({
    type: 'SET_START_DATE',
    startDate
});

const setEndDate = (endDate) => ({
    type: 'SET_END_DATE',
    endDate
});

export {setTextFilter, resetAllFilters, setVisibilityFilter, sortByDate, sortByWeight, setStartDate, setEndDate}