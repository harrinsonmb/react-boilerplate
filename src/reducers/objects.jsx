const objectsReducerDefaultState = [];

const objectsReducer = (( state = objectsReducerDefaultState , action ) => {
    switch (action.type) {
        case 'ADD_OBJECT':
            return [
                ...state,
                action.object
            ];
        case 'REMOVE_OBJECT':
            return state.filter(({ id }) => id !== action.id);
        case 'UPDATE_OBJECT':
            return state.map((object) => {
                return object.id === action.id ? { ...object, ...action.updates } : object;
            });
        default:
            return state;
    }
});

export default objectsReducer;