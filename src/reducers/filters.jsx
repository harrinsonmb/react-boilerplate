import moment from "moment";

const filterReducerDefaultState = {
    text: '',
    sortBy: 'date',
    privateHidden: 'false',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
};

export default (state = filterReducerDefaultState, action) => {
    switch (action.type) {
        case 'SET_START_DATE':
            return {
                ...state,
                startDate: action.startDate
            };
        case 'SET_END_DATE':
            return {
                ...state,
                endDate: action.endDate
            };
        case 'SET_TEXT_FILTER':
            return {
                ...state,
                text: action.text
            };
        case 'SET_VISIBILITY':
            return {
                ...state,
                hidePrivateEnabled: action.privateObjectsHidden
            };
        case 'SORT_BY_WEIGHT':
            return {
                ...state,
                sortBy: 'weight'
            };
        case 'SORT_BY_DATE':
            return {
                ...state,
                sortBy: 'date'
            };
        case 'RESET_FILTERS':
            return filterReducerDefaultState;
        default:
            return state;
    }
}