const getVisibleObjects = (expenses, { text, sortBy, hidePrivateEnabled, startDate, endDate}) => {
    return expenses.filter((expense) => {
        const startDateMatch = typeof startDate !== 'number' || expense.createdAt >= startDate;
        const endDateMatch = typeof endDate !== 'number' || expense.createdAt <= endDate;
        const textMatch = expense.description.toLowerCase().includes(text.toLowerCase());
        const visibilityMatch = expense.is_public || !hidePrivateEnabled;

        return startDateMatch && endDateMatch && textMatch && visibilityMatch;
    }).sort( (a, b) => {
        if (sortBy === 'date'){
            return a.createdAt - b.createdAt;
        }else if (sortBy === 'weight'){
            return a.weight - b.weight;
        }else {
            return 0
        }
    });
};

export default getVisibleObjects;
