import {combineReducers, createStore} from "redux";

import filtersReducer from "../reducers/filters";
import objectsReducer from "../reducers/objects";

export default () => {
    return createStore(
        combineReducers({
            filters: filtersReducer,
            objects: objectsReducer
        })
    );
}